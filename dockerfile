FROM node:18.8.0-alpine3.16 AS builder

# set working directory
WORKDIR /app

# add app
COPY . ./

RUN npm install
RUN npm run build

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

COPY --from=builder /app/out ./

ENTRYPOINT ["nginx", "-g", "daemon off;"]

